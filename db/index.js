const mysql =require('mysql8')
const db = mysql.createPool({
	host:'localhost',
	user:'back_system',
	password:'123456',
	database:'back_system',
})
db.getConnection((err,connection)=>{
	if(err){
		console.error('Error connection to MySQL'+ err.stack);
		console.log('数据库连接失败')
		return
	}
	console.log('MySQL数据库连接成功,连接ID '+ connection.threadId)
})
module.exports= db