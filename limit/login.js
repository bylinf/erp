//
const joi=require('joi')
const account = joi.string().alphanum().min(6).max(12).required()
const password = joi.string().pattern(/^[A-Za-z\d@!%*?&]{8,}$/).required()

exports.login_limit={
	body:{account,password}
}