/**注册登录路由处理函数
 ** param：req前端过来的数据
 ** param：res返回给前端的数据
 ** sql语句：db.query(执行的语句,参数,处理结果的函数)
 **/
const db = require('../db/index.js')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const jwtconfig = require('../jwt_config/index.js')

//验证码
const svgCaptcha = require('svg-captcha')
//转化base64url
// const base64url = require('base64url')
let allSession = {}

exports.captcha = (req, res) => {
	//生成验证码,string&svg
	const options = {
		size: 4,
		ignoreChars: '0o1ilIqa',
		width: 100,
		height: 30,
		color: true,
		fontSize: 40,
		noise: 0,
		inverse: false
	}
	// const ca = svgCaptcha.create(options)
	const ca = svgCaptcha.createMathExpr(options)
	allSession[req.sessionID]=ca.text
	return res.send({ code: 200, message:'验证码请求成功',type: 'svg', data: { code:200,captcha: ca.data, captchaSession: req.sessionID } })
}

//公用代码
const queryUserSql = 'SELECT * FROM users WHERE account = ?'
const insertUserSql = 'INSERT INTO users set ?'

//数据验证
const checkRequest = (req, res) => {
	const {account,password,captchaString,captchaSession} = req.body
	//账户检查
	if (!account || !password || !captchaString || !captchaSession) return res.cc('存在空的提交内容')
	//密码检查
	const reg_password = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[^\da-zA-Z\s]).{8,}$/g
	if (!reg_password.test(password)) return res.cc('密码是8-20个字母、数字、符号字符组合')
	//验证码检查
	if (captchaString !== allSession[captchaSession]) {
		delete allSession[captchaSession]
		return res.cc('验证码错误')
	}
	delete allSession[captchaSession]
  return true
}
//注册账户
exports.register = async (req, res) => {
	//step1：数据校验
	if(!checkRequest(req, res)) return
	try {
		// step2，查询数据
		const results = await new Promise((resolve, reject) => {
			db.query(queryUserSql, req.body.account, (err, results) => {
				if (err) reject(err)
				resolve(results)
			})
		})
		//step3,存在用户
		if (results.length > 0) return res.cc('账户已存在')
		//step4：加密密码
		req.body.password = bcrypt.hashSync(req.body.password, 10)
		//用户默认注册的身份
		const identity = '用户'
		//创建时间
		const create_time = new Date()
		//创建用户
		const insertData = {
			account: req.body.account,
			password: req.body.password,
			identity,
			create_time,
			status: 0
		}
		await new Promise((resolve, reject) => {
			db.query(insertUserSql, insertData, (err, results) => {
				if (err) reject(err)
				if (results.affectedRows !== 1) return res.cc('注册失败')
				return res.send({ code: 200, message: '注册成功' })
			})
		})
		console.log(results)
	} catch (err) { return }
}
//账户登录
exports.login = async  (req, res) => {
	//step1，校验数据
	if(!checkRequest(req, res)) return
	try {
		// step2，查询数据
		const results = await new Promise((resolve, reject) => {
			db.query(queryUserSql, req.body.account, (err, results) => {
				if (err) reject(err)
				resolve(results)
			})
		})
		//step3,用户不存在
		if (results.length === 0) return res.cc('账户不存在')
		//setp3,数据故障
		if (results.length !== 1) return res.cc('数据异常，请联系管理员')
		//step4，校验密码
		if (!bcrypt.compareSync(req.body.password, results[0].password)) return res.cc('密码错误,请重输')
		//step5，账户状态
		if (results[0].status === '1') return res.cc('账户未启用，请联系管理员')
		if (results[0].status === '2') return res.cc('账户未审核，请联系管理员')
		if (results[0].status === '3') return res.cc('账户冻结中，请联系管理员')
		if (results[0].status === '4') return res.cc('账户审核未通过，请联系管理员')
		//step7,生成token
		const userinfo = { ...results[0], password: '', create_time: '', update_time: '', status: '', avatar_url: '' }
		const token = jwt.sign(userinfo, jwtconfig.jwtSecretKey, { expiresIn: '30s' })
		return res.send({ code: 200, message: '登录成功', data: { code:200,userinfo, token: `Bearer ${token}` } })
	} catch (err) {return}
}