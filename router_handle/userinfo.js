const db = require('../db/index.js')
const bcrypt = require('bcryptjs')
const crypto = require('crypto')
const fs=require('fs')

exports.uploadAvatar = (req,res)=>{
  const only_id = crypto.randomUUID()
  let oldname=req.files[0].filename
  let newname = Buffer.from(req.files[0].originalname,'latin1').toString('utf8')
  fs.renameSync('./public/upload/'+oldname,'./public/upload/'+newname)
 
  const sql = 'insert into image set ?'
  db.query(sql,
    {
      image_url: 'http://localhost:3000/upload/' + newname,
      only_id
  }),(err,result)=>{
    if(err) return res.cc(err)
    console.log(result)
    // res.send({
    //   only_id,
    //   status:0,
    //   url: 'http://localhost:3000/upload/' + newname
    // })
  }
}