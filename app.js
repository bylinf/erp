//指定监听的端口
const port = '3000';

//导入exporess
const express = require('express')
const app = express()

//全局挂载cors跨域
const cors = require('cors')
app.use(cors())

//文件上传
const multer = require('multer')
const upload = multer({ dest: './public/upload' })
app.use(upload.any())
//处理静态资源
app.use(express.static('./public'))

//表单数据处理 bodyParser
const bodyParser = require('body-parser');
//parse application/json
app.use(bodyParser.json())
//parse application/x-www-form-urlencoded
//param: false--值为数组，字符串
//param: true--值为任意类型
app.use(bodyParser.urlencoded({
	extended: false
}))
//cookie解析
const session = require('express-session')
const cookieParser = require('cookie-parser')
app.use(cookieParser())
app.use(session({
	secret:'bylinf',
	resave:false,
	saveUninitialized:true,
	cookie: { secure: false, maxAge:1000*60*3},
	rolling:true,
	name:'lfsession'
}))

//路由拦截中间件,统一处理错误
app.use((req, res, next) => {
	res.cc = (err, code = 1) => {
		res.send({
			code,
			message: err instanceof Error ? err.message : err
		})
	}
	next()
})

//token相关
const jwtconfig = require('./jwt_config/index.js')
const {
	expressjwt: jwt
} = require('express-jwt')

app.use(jwt({
	secret: jwtconfig.jwtSecretKey,
	algorithms: ['HS256'],
}).unless({
	path: [/^\/api\//]
}))
//引入路由
const routes = require('./router/index.js')
app.use('/',routes)
//验证表单，不符合joi规则报错
app.use((req, res, next) => {
	if(err instanceof joi.ValidationError) return res.cc(err)
	next()
})


//绑定侦听指定端口
app.listen(port, () => {
	console.log(`http://localhost:${port}`)
})