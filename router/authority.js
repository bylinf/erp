//登录注册模块路由
const express = require('express')
const router = express.Router()
// const expressJoi = require('@escook/express-joi')
// const {
// 	login_limit
// } = require('../limit/login.js')

const authorityHandler = require('../router_handle/authority')
//注册登录函数
router.post('/login', authorityHandler.login)
router.post('/register', authorityHandler.register)
//验证码函数
router.get('/captcha', authorityHandler.captcha)
module.exports = router