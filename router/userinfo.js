//登录注册模块路由
const express = require('express')
const router = express.Router()
// const expressJoi = require('@escook/express-joi')
// const {
// 	login_limit
// } = require('../limit/login.js')
//用户路由处理函数
const userinfoHandler = require('../router_handle/userinfo')
router.post('/uploadAvatar', userinfoHandler.uploadAvatar)
module.exports = router