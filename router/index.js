const express = require('express')
const routes = express.Router()

//权限路由
const authorityRouter = require('./authority.js')
routes.use('/api', authorityRouter)
//用户路由
const userinfoRouter = require('./userinfo.js')
routes.use('/user', userinfoRouter)
//测试路由
const testRouter = require('./test.js')
routes.use('/api', testRouter)
module.exports = routes