//登录注册模块路由
const express = require('express')
const router = express.Router()
const testHandler = require('../router_handle/test')
router.get('/test', testHandler.get)
router.post('/test', testHandler.test)
module.exports = router